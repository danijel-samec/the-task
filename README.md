# Frontend Developer Task

## The Task
The component is displaying the very next race with the highest prize money, based on the
user’s preferred race type. The user also has the ability to select what type of races
he/she prefers to see.

### The components has the following functionality:
* Displays the very next race
* Displayed race is filtered by race type
* Able to receive updated data at any time
* Filter for each race type can be switched on/off independently from each other,
enabling any combination of active race types
* Race type selection persist even after page refresh
* Has an effective display and user experience

## Installation
```
npm i
```
## Run
```
npm start
```
## Build (optional)
```
npm run build
```
## Test (optional)
```
npm test
```

NOTE: Application is not using current time because database data is in past.
```
Comment line 50 in: src/pages/Home/Home.js 
Uncomment line 52 in: src/pages/Home/Home.js 
```

## Q&A
**Q: How long did you spend on the test? Would you do anything differently if you had more time?**  
A: I spent few days developing "The Task" project, I would probably use different type-checking method if I had more time.   

**Q: In what ways would you adapt your component so that it could be
used in many different scenarios where this component is required?**  
A: I would adapt component to be box/panel widget with optional children props. With that approach component can accept and render children component.

**Q: What is your favorite CSS property? Why?**  
A: My favorite CSS property is "display: flex;" because it defines a flex container and enable us to implement flexbox layout and use properties like align-items, flex-direction, justify-content, etc.  

**Q: What is your favorite modern JavaScript feature? Why?**  
A: My favorite modern JavaScript feature is arrow function, reason why is because arrow function simplify function scoping and the this keyword.
