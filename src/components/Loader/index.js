import React from "react";

import "./index.scss";

const Loader = () => (
  <div className="loader">
    <div>
      <div className="spinner">
        <div className="cube1" />
        <div className="cube2" />
      </div>
    </div>
  </div>
);

export default Loader;
