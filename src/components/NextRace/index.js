import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";

import Loader from "../Loader";

import "./index.scss";

class NextRace extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { data, onClickRunnerOdds, showLoader } = this.props;

    if (showLoader) {
      return (
        <div className="panel next-race">
          <Loader />
        </div>
      );
    }

    if (data) {
      const { country, subtitle, title, postTime, raceType, runners } = data;
      const countryFlag = `flag-icon-${country.toLowerCase()}`;

      return (
        <div className="panel next-race">
          <div className="next-race__head">
            <div className="f-row">
              <span className={`flag-icon ${countryFlag} country`} />
              <span className="title">{title}</span>
            </div>

            <div className="time">{moment(postTime * 1000).fromNow()}</div>
          </div>

          <div className="next-race__head hr">
            <div className="f-row">
              <span className="subtitle">{subtitle}</span>
            </div>

            <span className="race-type">{raceType}</span>
          </div>

          <div className="next-race__body">
            {runners.map(({ id, name, odds }) => (
              <div className="f-row runner" key={id}>
                <h6>{name}</h6>
                {onClickRunnerOdds && (
                  <button onClick={() => onClickRunnerOdds(id)}>{odds}</button>
                )}
              </div>
            ))}
          </div>
        </div>
      );
    }

    return null;
  }
}

NextRace.propTypes = {
  data: PropTypes.exact({
    country: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    raceType: PropTypes.string.isRequired,
    runners: PropTypes.arrayOf(
      PropTypes.exact({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        odds: PropTypes.number.isRequired,
      }),
    ),
    postTime: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  }),
  onClickRunnerOdds: PropTypes.func,
  showLoader: PropTypes.bool,
};

export default NextRace;
