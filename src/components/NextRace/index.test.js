import React from "react";
import renderer from "react-test-renderer";

import NextRace from "./index";

const nextRace = {
  country: "US",
  postTime: 1544009760,
  raceType: "T",
  title: "Race title",
  subtitle: "Race subtitle",
  runners: [
    {
      id: 1,
      name: "Runner 1",
      odds: 2,
    },
    {
      id: 2,
      name: "Runner 2",
      odds: 3,
    },
    {
      id: 3,
      name: "Runner 3",
      odds: 2,
    },
  ],
};

describe("The NextRace component", () => {
  it("renders loader", () => {
    const tree = renderer.create(<NextRace showLoader />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders nothing", () => {
    const tree = renderer.create(<NextRace data={null} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders race", () => {
    const tree = renderer
      .create(<NextRace data={nextRace} onClickRunnerOdds={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
