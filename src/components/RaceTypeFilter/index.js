import React, { Component } from "react";
import PropTypes from "prop-types";

import "./index.scss";

const RaceTypeButton = ({ statusColor, isActive, onClick, type }) => (
  <button
    className="f-row race-type-button"
    onClick={() => onClick(type)}
    style={isActive ? { backgroundColor: statusColor, color: "#ffffff" } : {}}>
    <span className="icon">{type}</span>
  </button>
);

RaceTypeButton.propTypes = {
  statusColor: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

const typeScheme = [
  {
    color: "#27ae60",
    type: "G",
  },
  {
    color: "#2980b9",
    type: "J",
  },
  {
    color: "#f1c40f",
    type: "T",
  },
  {
    color: "#e74c3c",
    type: "D",
  },
];

class RaceTypeFilter extends Component {
  render() {
    const { activeTypes, onClickRaceType } = this.props;

    return (
      <div className="panel race-type-filter">
        {typeScheme.map(({ color, type }) => (
          <RaceTypeButton
            key={type}
            isActive={activeTypes.indexOf(type) !== -1}
            onClick={onClickRaceType}
            statusColor={color}
            type={type}
          />
        ))}
      </div>
    );
  }
}

RaceTypeFilter.propTypes = {
  activeTypes: PropTypes.arrayOf(PropTypes.string.isRequired),
  onClickRaceType: PropTypes.func.isRequired,
};

export default RaceTypeFilter;
