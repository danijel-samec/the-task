import React from "react";
import renderer from "react-test-renderer";

import RaceTypeFilter from "./index";

const EMPTY_FUNC = () => {};

describe("The RaceTypeFilter component", () => {
  it("renders buttons disabled", () => {
    const activeTypes = [];
    const tree = renderer
      .create(
        <RaceTypeFilter
          activeTypes={activeTypes}
          onClickRaceType={EMPTY_FUNC}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders buttons partial active ", () => {
    const activeTypes = ["J", "D"];
    const tree = renderer
      .create(
        <RaceTypeFilter
          activeTypes={activeTypes}
          onClickRaceType={EMPTY_FUNC}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders buttons active", () => {
    const activeTypes = ["G", "J", "T", "D"];
    const tree = renderer
      .create(
        <RaceTypeFilter
          activeTypes={activeTypes}
          onClickRaceType={EMPTY_FUNC}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
