import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import actions from "@store/actions";

import NextRace from "@components/NextRace";
import RaceTypeFilter from "@components/RaceTypeFilter";

import { getNextRace, mapNextRace } from "./Home.utils";
import "./Home.scss";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.interval = null;
    this.watchTime = this.watchTime.bind(this);
  }

  componentDidMount() {
    this.watchTime();
    this.props.getDataAsync();
  }

  componentWillUnmount() {
    if (this.interval !== null) {
      clearInterval(this.interval);
    }
  }

  watchTime() {
    this.interval = setInterval(() => {
      const time = new Date().getTime();

      this.props.setTime(time);
    }, 1000);
  }

  render() {
    const {
      data,
      isFetching,
      raceTypes,
      time,
      getDataAsync,
      updateRaceTypes,
    } = this.props;
    const fakeTime = new Date("2 dec 2018").getTime();
    const nextRace = getNextRace(data, raceTypes, fakeTime / 1000);
    // TODO: Use "time" instead when API database is updated
    // const nextRace = getNextRace(data, raceTypes, time / 1000);

    return (
      <div className="home">
        <h1>{`THE TASK - ${moment(time).format("H:mm")} h`}</h1>

        <RaceTypeFilter
          activeTypes={raceTypes}
          onClickRaceType={updateRaceTypes}
        />

        <NextRace
          data={mapNextRace(nextRace)}
          onClickRunnerOdds={console.log}
          showLoader={isFetching}
        />

        <button className="panel fetch-button" onClick={getDataAsync}>
          Fetch
        </button>
      </div>
    );
  }
}

Home.propTypes = {
  data: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  raceTypes: PropTypes.array.isRequired,
  time: PropTypes.number.isRequired,
  getDataAsync: PropTypes.func.isRequired,
  setTime: PropTypes.func.isRequired,
  updateRaceTypes: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.races.data,
  isFetching: state.races.isFetching,
  raceTypes: state.races.raceTypes,
  time: state.common.time,
});

const mapDispatchToProps = (dispatch) => ({
  getDataAsync: (val) => dispatch(actions.getDataAsync(val)),
  setTime: (val) => dispatch(actions.setTime(val)),
  updateRaceTypes: (val) => dispatch(actions.updateRaceTypes(val)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
