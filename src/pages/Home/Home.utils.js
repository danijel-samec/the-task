// Custom currency converter
export const convertCurrencyToEUR = ({ amount, currency }) => {
  switch (currency) {
    case "AUD":
      return amount * 0.63;
    case "HKD":
      return amount * 0.11;
    case "SEK":
      return amount * 0.096;
    case "TRY":
      return amount * 0.16;
  }

  return amount;
};

export const getNextRace = (races, raceTypes, unixTimestamp) => {
  const filteredAndSorted = races
    // As a user I would like to only see the next race which has a race type
    // I’ve set to active in my filters.
    .filter(({ race_type }) => raceTypes.indexOf(race_type) !== -1)
    // very next race compared to my local time
    .filter(({ post_time }) => post_time > unixTimestamp)
    // As a user I would like to see one race displayed which is the closest.
    .sort((a, b) => a.post_time - b.post_time);

  if (filteredAndSorted.length > 0) {
    const closestRaceType = filteredAndSorted[0].race_type;

    const byPrize = filteredAndSorted
      // If there are more races with the same race type.
      .filter(({ race_type }) => race_type === closestRaceType)
      // As a user I would like to see the race with the highest prize money
      .sort(
        (a, b) => convertCurrencyToEUR(b.purse) - convertCurrencyToEUR(a.purse),
      );

    if (byPrize.length > 0) {
      // return first item
      return byPrize[0];
    }
  }

  return null;
};

export const mapNextRace = (data) => {
  try {
    const { event, num_runners, post_time, purse, race_type, runners } = data;
    return {
      country: event.country,
      postTime: post_time,
      raceType: race_type,
      runners: runners.map(({ id_runner, name, odds }) => ({
        id: id_runner,
        name,
        odds,
      })),
      subtitle: `${num_runners} runners, ${purse.amount} ${purse.currency}`,
      title: event.title,
    };
  } catch (e) {
    // no-op
  }

  return null;
};
