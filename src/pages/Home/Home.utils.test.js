import * as utils from "./Home.utils";

const unixTimestamp = 1544009300;
const createRace = (race_type, post_time, amount, currency) => ({
  race_type,
  post_time,
  purse: {
    amount: amount || 120,
    currency: currency || "EUR",
  },
});

test("Race with the highest prize money", () => {
  const raceTypes = ["J"];
  const races = [
    createRace("J", unixTimestamp + 5),
    createRace("J", unixTimestamp + 5, 10000, "HKD"),
    createRace("J", unixTimestamp + 5, 15000, "SEK"),
    createRace("J", unixTimestamp + 5, 13000, "SEK"),
  ];
  const expected = races[2];

  expect(utils.getNextRace(races, raceTypes, unixTimestamp)).toEqual(expected);
});

test("Empty race types", () => {
  const raceTypes = [];
  const races = [
    createRace("J", unixTimestamp + 5),
    createRace("G", unixTimestamp + 10),
  ];
  const expected = null;

  expect(utils.getNextRace(races, raceTypes, unixTimestamp)).toEqual(expected);
});

test("Past races", () => {
  const raceTypes = ["T", "G", "J"];
  const races = [
    createRace("T", unixTimestamp - 5),
    createRace("G", unixTimestamp - 10),
    createRace("J", unixTimestamp - 15),
  ];
  const expected = null;

  expect(utils.getNextRace(races, raceTypes, unixTimestamp)).toEqual(expected);
});

test("Next race", () => {
  const raceTypes = ["T", "G", "J"];
  const races = [
    createRace("T", unixTimestamp + 10),
    createRace("G", unixTimestamp - 10),
    createRace("J", unixTimestamp - 15),
    createRace("T", unixTimestamp + 5),
  ];
  const expected = races[3];

  expect(utils.getNextRace(races, raceTypes, unixTimestamp)).toEqual(expected);
});

test("Race with specific type", () => {
  const raceTypes = ["G"];
  const races = [
    createRace("T", unixTimestamp + 5),
    createRace("J", unixTimestamp - 15),
    createRace("G", unixTimestamp + 10),
  ];
  const expected = races[2];

  expect(utils.getNextRace(races, raceTypes, unixTimestamp)).toEqual(expected);
});
