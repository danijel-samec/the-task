const API = "http://www.mocky.io/v2/";
const ID = "5c07be033000006217d25cba";

export const handle = (promise) =>
  promise.then((data) => [null, data]).catch((err) => [err]);

export const handleServerResponse = async (promise) => {
  const [err, response] = await handle(promise);

  if (err !== null) return [err];

  return await handle(response.json());
};

export const fetchRaces = async () => {
  const request = fetch(`${API}${ID}`);

  const [err, response] = await handleServerResponse(request);
  if (!err && response.data) {
    return response.data;
  }

  return [];
};
