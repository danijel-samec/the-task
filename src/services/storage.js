export const setRaceTypes = (data) => {
  sessionStorage.setItem("@TT:race-types", JSON.stringify(data));
};

export const getRaceTypes = () => {
  const data = sessionStorage.getItem("@TT:race-types");

  if (data !== null) {
    return JSON.parse(data);
  }

  return null;
};
