import * as commonActions from "./common/actions";
import * as racesActions from "./races/actions";

export default {
  ...commonActions,
  ...racesActions,
};
