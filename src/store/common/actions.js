import TYPES from "./types";

export const setTime = (payload) => ({
  type: TYPES.SET_TIME,
  payload,
});
