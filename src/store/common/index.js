import TYPES from "./types";
import initialState from "./state";

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.SET_TIME: {
      return {
        ...state,
        time: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
