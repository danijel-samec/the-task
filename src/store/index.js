import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import * as api from "@services/api";
import * as storage from "@services/storage";
import commonReducers from "./common";
import racesReducers from "./races";

const reducers = combineReducers({
  common: commonReducers,
  races: racesReducers,
});

const store = createStore(
  reducers,
  applyMiddleware(thunk.withExtraArgument(api)),
);

store.subscribe(() => {
  storage.setRaceTypes(store.getState().races.raceTypes);
});

export default store;
