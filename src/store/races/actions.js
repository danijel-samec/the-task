import TYPES from "./types";

export const setData = (payload) => ({
  type: TYPES.SET_DATA,
  payload,
});

export const setIsFetching = (payload) => ({
  type: TYPES.SET_IS_FETCHING,
  payload,
});

export const updateRaceTypes = (payload) => ({
  type: TYPES.UPDATE_RACE_TYPES,
  payload,
});

export const getDataAsync = () => {
  return async (dispatch, _, api) => {
    dispatch(setIsFetching(true));
    const data = await api.fetchRaces();

    dispatch(setData(data));
    dispatch(setIsFetching(false));
  };
};
