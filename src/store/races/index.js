import TYPES from "./types";
import initialState from "./state";
import * as utils from "./utils";

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.SET_DATA: {
      return {
        ...state,
        data: action.payload,
      };
    }
    case TYPES.SET_IS_FETCHING: {
      return {
        ...state,
        isFetching: action.payload,
      };
    }
    case TYPES.UPDATE_RACE_TYPES: {
      return {
        ...state,
        raceTypes: utils.updateArrayItem(action.payload, state.raceTypes),
      };
    }
    default: {
      return state;
    }
  }
};
