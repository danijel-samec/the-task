import * as storage from "@services/storage";

const raceTypes = storage.getRaceTypes();

export default {
  data: [],
  isFetching: false,
  raceTypes: raceTypes || [],
};
