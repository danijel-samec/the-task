export default {
  SET_DATA: "[RACES] SET_DATA",
  SET_IS_FETCHING: "[RACES] SET_IS_FETCHING",
  UPDATE_RACE_TYPES: "[RACES] UPDATE_RACE_TYPES",
};
