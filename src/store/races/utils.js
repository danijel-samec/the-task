export const updateArrayItem = (item, array) => {
  if (array.indexOf(item) === -1) {
    return [...array, item];
  }

  return array.filter((current) => current !== item);
};
