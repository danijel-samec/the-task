import * as utils from "./utils";

const mockRaceTypes = ["T", "J"];

test("Remove race type from array", () => {
  const raceType = "T";
  const expected = ["J"];

  expect(utils.updateArrayItem(raceType, mockRaceTypes)).toEqual(expected);
});

test("Add new race type to array", () => {
  const raceType = "D";
  const expected = ["T", "J", "D"];

  expect(utils.updateArrayItem(raceType, mockRaceTypes)).toEqual(expected);
});
